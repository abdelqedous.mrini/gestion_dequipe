<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGoalsAndTrophiesToJoueursTable extends Migration
{
    public function up()
    {
        Schema::table('joueurs', function (Blueprint $table) {
            $table->integer('nombre_buts')->default(0);
            $table->integer('nombre_trophees')->default(0);
        });
    }

    public function down()
    {
        Schema::table('joueurs', function (Blueprint $table) {
            $table->dropColumn('nombre_buts');
            $table->dropColumn('nombre_trophees');
        });
    }
}
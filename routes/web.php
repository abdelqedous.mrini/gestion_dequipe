<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JoueurController;
use App\Http\Controllers\EquipeController;
use App\Http\Controllers\FiltrageController;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

// Routes des joueurs
Route::resource('joueurs', JoueurController::class);

// Routes des équipes
Route::resource('equipes', EquipeController::class);

// Route de filtrage
Route::get('/filtrage', [FiltrageController::class, 'filtrer'])->name('filter');

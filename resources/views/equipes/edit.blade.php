<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier une équipe</title>
</head>
<body>
    <h1>Modifier une équipe</h1>

    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('equipes.update', $equipe->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="nom_equipe">Nom de l'équipe:</label>
            <input type="text" id="nom_equipe" name="nom_equipe" value="{{ $equipe->nom_equipe }}" required>
        </div>
        <div>
            <label for="nationalite">Nationalité:</label>
            <input type="text" id="nationalite" name="nationalite" value="{{ $equipe->nationalite }}" required>
        </div>
        <button type="submit">Enregistrer les modifications</button>
    </form>
</body>
</html>

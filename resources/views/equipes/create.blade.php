<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter une équipe</title>
</head>
<body>
    <h1>Ajouter une équipe</h1>

    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('equipes.store') }}" method="POST">
        @csrf
        <div>
            <label for="nom_equipe">Nom de l'équipe:</label>
            <input type="text" id="nom_equipe" name="nom_equipe" required>
        </div>
        <div>
            <label for="nationalite">Nationalité:</label>
            <input type="text" id="nationalite" name="nationalite" required>
        </div>
        <button type="submit">Ajouter Équipe</button>
    </form>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des équipes</title>
</head>
<body>
    <h1>Liste des équipes</h1>

    <ul>
        @foreach ($equipes as $equipe)
            <li>
                {{ $equipe->nom_equipe }} - {{ $equipe->nationalite }}
                <form action="{{ route('equipes.destroy', $equipe->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Supprimer</button>
                </form>
                <a href="{{ route('equipes.edit', $equipe->id) }}">Modifier</a>
            </li>
        @endforeach
    </ul>

    <a href="{{ route('equipes.create') }}">Ajouter une équipe</a>
</body>
</html>

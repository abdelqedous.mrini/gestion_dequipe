<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>Bienvenue sur notre application de gestion des joueurs et des équipes</h1>
    
    <div>
        <a href="{{ route('joueurs.index') }}">Voir la liste des joueurs</a>
    </div>
    
    <div>
        <a href="{{ route('equipes.index') }}">Voir la liste des équipes</a>
    </div>

    <h2>Filtrer les joueurs et les équipes</h2>
    <form action="{{ route('filter') }}" method="GET">
        <input type="text" name="nom_joueur" placeholder="Nom du joueur">
        <input type="text" name="prenom_joueur" placeholder="Prénom du joueur">
        <input type="number" name="nombre_buts" placeholder="Nombre de buts">
        <input type="number" name="nombre_trophees" placeholder="Nombre de trophées">
        <input type="text" name="nationalite" placeholder="Nationalité du joueur">
        <input type="text" name="equipe" placeholder="Nom de l'équipe">
        <input type="number" name="nombre_buts_equipe" placeholder="Nombre de buts par équipe">
        <button type="submit">Filtrer</button>
    </form>



</body>
</html>

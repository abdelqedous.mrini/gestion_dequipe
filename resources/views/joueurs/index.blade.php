<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des joueurs</title>
</head>
<body>
    <h1>Liste des joueurs</h1>
    <a href="{{ route('joueurs.create') }}">Ajouter un joueur</a>
    <ul>
        @foreach ($joueurs as $joueur)
            <li>
                {{ $joueur->nom }} {{ $joueur->prenom }}
                <form action="{{ route('joueurs.destroy', $joueur->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Supprimer</button>
                </form>
                <a href="{{ route('joueurs.edit', $joueur->id) }}">Modifier</a>
                <a href="{{ route('joueurs.show', $joueur->id) }}">Détails</a>
            </li>
        @endforeach
    </ul>

    
</body>
</html>

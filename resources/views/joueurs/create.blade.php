<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un joueur</title>
</head>
<body>
    <h1>Ajouter un joueur</h1>

    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('joueurs.store') }}" method="POST">
        @csrf
        <div>
            <label for="nom">Nom:</label>
            <input type="text" id="nom" name="nom" required>
        </div>
        <div>
            <label for="prenom">Prénom:</label>
            <input type="text" id="prenom" name="prenom" required>
        </div>
        <div>
            <label for="age">Âge:</label>
            <input type="number" id="age" name="age" required>
        </div>
        <div>
            <label for="nationalite">Nationalité:</label>
            <input type="text" id="nationalite" name="nationalite" required>
        </div>
        <div>
            <label for="equipe_id">Équipe:</label>
            <select id="equipe_id" name="equipe_id" required>
                @foreach($equipes as $equipe)
                    <option value="{{ $equipe->id }}">{{ $equipe->nom_equipe }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="nombre_buts">Nombre de buts marqués:</label>
            <input type="number" id="nombre_buts" name="nombre_buts" min="0">
        </div>
        <div>
            <label for="nombre_trophees">Nombre de trophées gagnés:</label>
            <input type="number" id="nombre_trophees" name="nombre_trophees" min="0">
        </div>
        <button type="submit">Ajouter Joueur</button>
    </form>
</body>
</html>

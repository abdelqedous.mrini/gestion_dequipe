<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Détails du joueur</title>
</head>
<body>
    <h1>Détails du joueur</h1>

    <p><strong>Nom:</strong> {{ $joueur->nom }}</p>
    <p><strong>Prénom:</strong> {{ $joueur->prenom }}</p>
    <p><strong>Âge:</strong> {{ $joueur->age }}</p>
    <p><strong>Nationalité:</strong> {{ $joueur->nationalite }}</p>
    <p><strong>Équipe:</strong> {{ $joueur->equipe ? $joueur->equipe->nom_equipe : 'Non assignée' }}</p>
    <p><strong>Nombre de buts marqués:</strong> {{ $joueur->nombre_buts }}</p>
    <p><strong>Nombre de trophées gagnés:</strong> {{ $joueur->nombre_trophees }}</p>

    <a href="{{ route('joueurs.index') }}">Retour à la liste des joueurs</a>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifier un joueur</title>
</head>
<body>
    <h1>Modifier un joueur</h1>

    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('joueurs.update', $joueur->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="nom">Nom:</label>
            <input type="text" id="nom" name="nom" value="{{ $joueur->nom }}" required>
        </div>
        <div>
            <label for="prenom">Prénom:</label>
            <input type="text" id="prenom" name="prenom" value="{{ $joueur->prenom }}" required>
        </div>
        <div>
            <label for="age">Âge:</label>
            <input type="number" id="age" name="age" value="{{ $joueur->age }}" required>
        </div>
        <div>
            <label for="nationalite">Nationalité:</label>
            <input type="text" id="nationalite" name="nationalite" value="{{ $joueur->nationalite }}" required>
        </div>
        <div>
            <label for="equipe_id">Équipe:</label>
            <select id="equipe_id" name="equipe_id" required>
                @foreach($equipes as $equipe)
                    <option value="{{ $equipe->id }}" {{ $joueur->equipe_id == $equipe->id ? 'selected' : '' }}>{{ $equipe->nom_equipe }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="nombre_buts">Nombre de buts marqués:</label>
            <input type="number" id="nombre_buts" name="nombre_buts" value="{{ $joueur->nombre_buts }}" min="0">
        </div>
        <div>
            <label for="nombre_trophees">Nombre de trophées gagnés:</label>
            <input type="number" id="nombre_trophees" name="nombre_trophees" value="{{ $joueur->nombre_trophees }}" min="0">
        </div>
        <button type="submit">Enregistrer les modifications</button>
    </form>
</body>
</html>

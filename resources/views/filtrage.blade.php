<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Résultats de la recherche</title>
</head>
<body>
    <h1>Résultats de la recherche</h1>

    @if ($resultats->isEmpty())
        <p>Aucun résultat trouvé.</p>
    @else
        <ul>
            @foreach ($resultats as $resultat)
                <li>{{ $resultat->nom }} {{ $resultat->prenom }} - Équipe: {{ $resultat->nom_equipe }} - Buts: {{ $resultat->nombre_buts }} - Trophées: {{ $resultat->nombre_trophees }}</li>
            @endforeach
        </ul>
    @endif
</body>
</html>

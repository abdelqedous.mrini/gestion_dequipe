<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Joueur extends Model
{
    protected $fillable = ['nom', 'prenom', 'age', 'nationalite', 'equipe_id', 'nombre_buts', 'nombre_trophees'];

    public function equipe()
    {
        return $this->belongsTo(Equipe::class);
    }
}

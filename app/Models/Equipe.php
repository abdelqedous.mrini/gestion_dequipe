<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    protected $fillable = ['nom_equipe', 'nationalite'];

    public function joueurs()
    {
        return $this->hasMany(Joueur::class);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Equipe;

class EquipeController extends Controller
{
    // Méthode pour afficher la liste des équipes
   public function index()
{
    $equipes = Equipe::all();
    return view('equipes.index', ['equipes' => $equipes]);
}

    // Méthode pour afficher le formulaire de création d'une équipe
    public function create()
    {
        return view('equipes.create');
    }

    // Méthode pour enregistrer une nouvelle équipe
    public function store(Request $request)
    {
        // Validation des données du formulaire
        $request->validate([
            'nom' => 'required|string|max:255',
            'pays' => 'required|string|max:255',
            // Ajoutez d'autres règles de validation au besoin
        ]);

        // Création d'une nouvelle équipe
        Equipe::create($request->all());

        return redirect()->route('equipes.index')->with('success', 'Équipe créée avec succès.');
    }

    // Méthode pour afficher le formulaire de modification d'une équipe
    public function edit($id)
    {
        $equipe = Equipe::findOrFail($id);
        return view('equipes.edit', compact('equipe'));
    }

    // Méthode pour mettre à jour les informations d'une équipe
    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required|string|max:255',
            'pays' => 'required|string|max:255',
            // Ajoutez d'autres règles de validation au besoin
        ]);

        $equipe = Equipe::findOrFail($id);
        $equipe->update($request->all());

        return redirect()->route('equipes.index')->with('success', 'Équipe mise à jour avec succès.');
    }

    // Méthode pour supprimer une équipe
    public function destroy($id)
    {
        $equipe = Equipe::findOrFail($id);
        $equipe->delete();

        return redirect()->route('equipes.index')->with('success', 'Équipe supprimée avec succès.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FiltrageController extends Controller
{
    public function filtrer(Request $request)
    {
        // Récupérer les données du formulaire de filtrage
        $nom_joueur = $request->input('nom_joueur');
        $prenom_joueur = $request->input('prenom_joueur');
        $nombre_buts = $request->input('nombre_buts');
        $nombre_trophees = $request->input('nombre_trophees');
        $nationalite = $request->input('nationalite');
        $equipe = $request->input('equipe');
        $nombre_buts_equipe = $request->input('nombre_buts_equipe');

        // Filtrer les joueurs et les équipes en fonction des critères de recherche
        $resultats = DB::table('joueurs')
            ->join('equipes', 'joueurs.equipe_id', '=', 'equipes.id')
            ->select('joueurs.*', 'equipes.nom_equipe')
            ->when($nom_joueur, function ($query) use ($nom_joueur) {
                return $query->where('joueurs.nom', 'like', "%$nom_joueur%");
            })
            ->when($prenom_joueur, function ($query) use ($prenom_joueur) {
                return $query->where('joueurs.prenom', 'like', "%$prenom_joueur%");
            })
            ->when($nombre_buts, function ($query) use ($nombre_buts) {
                return $query->where('joueurs.nombre_buts', $nombre_buts);
            })
            ->when($nombre_trophees, function ($query) use ($nombre_trophees) {
                return $query->where('joueurs.nombre_trophees', $nombre_trophees);
            })
            ->when($nationalite, function ($query) use ($nationalite) {
                return $query->where('joueurs.nationalite', 'like', "%$nationalite%");
            })
            ->when($equipe, function ($query) use ($equipe) {
                return $query->where('equipes.nom_equipe', 'like', "%$equipe%");
            })
            ->when($nombre_buts_equipe, function ($query) use ($nombre_buts_equipe) {
                return $query->groupBy('equipes.id')
                    ->havingRaw('SUM(joueurs.nombre_buts) >= ?', [$nombre_buts_equipe]);
            })
            ->get();

        // Retourner la vue avec les résultats de la recherche
        return view('filtrage', compact('resultats'));
    }
}

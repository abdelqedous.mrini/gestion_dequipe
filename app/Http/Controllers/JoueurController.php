<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Joueur;
use App\Models\Equipe;


class JoueurController extends Controller
{
    public function index()
    {
        $joueurs = Joueur::all();
        return view('joueurs.index', compact('joueurs'));
    }

    public function create()
{
    $equipes = Equipe::all();
    return view('joueurs.create', compact('equipes'));
}


    public function store(Request $request)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'age' => 'required|integer',
            'nationalite' => 'required',
            'nombre_buts' => 'nullable|integer',
            'nombre_trophees' => 'nullable|integer'
        ]);

        Joueur::create($request->all());

        return redirect()->route('joueurs.index')->with('success', 'Joueur ajouté avec succès.');
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nom' => 'required',
            'prenom' => 'required',
            'age' => 'required|integer',
            'nationalite' => 'required',
            'nombre_buts' => 'nullable|integer',
            'nombre_trophees' => 'nullable|integer'
        ]);

        $joueur = Joueur::findOrFail($id);
        $joueur->update($request->all());

        return redirect()->route('joueurs.index')->with('success', 'Joueur mis à jour avec succès.');
    }

    public function destroy($id)
    {
        $joueur = Joueur::findOrFail($id);
        $joueur->delete();

        return redirect()->route('joueurs.index')->with('success', 'Joueur supprimé avec succès.');
    }
    public function show($id)
{
    $joueur = Joueur::findOrFail($id);
    return view('joueurs.show', compact('joueur'));
}
public function edit($id)
{
    $joueur = Joueur::findOrFail($id);
    $equipes = Equipe::all(); // Assurez-vous d'importer le modèle Equipe
    return view('joueurs.edit', compact('joueur', 'equipes'));
}

}
